const supertest = require('supertest')
const env = require('dotenv').config()

const api = supertest(process.env.BASE_URL)

//Hit API Delete Booking
const deleteBooking = (username, password, bookingID) =>
    api
        .delete('/booking/' + bookingID)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .auth(username,password)

module.exports = {
    deleteBooking
}