const supertest = require('supertest')
const env = require('dotenv').config()

const api = supertest(process.env.BASE_URL)

//Hit API Create Booking
const createBooking = (bodyRequest) =>
    api
        .post('/booking')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .send(bodyRequest)

module.exports = {
    createBooking
}