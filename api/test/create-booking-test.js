const assert = require('chai').expect;
const chai = require('chai');
chai.use(require('chai-json-schema'));

const env = require('dotenv').config()

const { createBooking } = require('../page/create-booking')
const schemaCreateBooking = require('../data/create-booking-schema.json');

const testCase = {
    "positive" : {
       "createBookingSuccess" : "As an User, I should be able to create a booking"
    },
    "negative" : {
       "failedBlankField" : "As a User, I should get error when I input blank field to create a booking"
    }
   }


describe(`@createBooking`, () => {

    describe(`Positive Case Create Booking`, () => {

        it(`@post ${testCase.positive.createBookingSuccess}`, async() => {

            const bodyRequest = {
                "firstname" : "Test",
                "lastname" : "Coba Mekari",
                "totalprice" : 12000,
                "depositpaid" : true,
                "bookingdates" : {
                    "checkin" : "2018-01-01",
                    "checkout" : "2019-01-01"
                },
                "additionalneeds" : "Breakfast"
            }
        
            const createResponse = await createBooking(bodyRequest)
            // console.log(createResponse)
            assert(createResponse.status).to.equal(200);
            assert(createResponse.body.booking.firstname).to.equal(bodyRequest.firstname);
            assert(createResponse.body.booking.lastname).to.equal(bodyRequest.lastname);
            assert(createResponse.body).to.be.jsonSchema(schemaCreateBooking);
        })
    
    })

    describe(`Negative Case Create Booking`, () => {

        it(`@post ${testCase.negative.failedBlankField}`, async() => {

            const bodyRequest = {
                "firstname" : "",
                "lastname" : "",
                "totalprice" : 12000,
                "depositpaid" : true,
                "bookingdates" : {
                    "checkin" : "2018-01-01",
                    "checkout" : "2019-01-01"
                },
                "additionalneeds" : "Breakfast"
            }
        
            const createResponse = await createBooking(bodyRequest)
            assert(createResponse.status).to.equal(500);
            assert(createResponse.text).to.equal('Internal Server Error');
        })
    
    })
})