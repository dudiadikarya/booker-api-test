const assert = require('chai').expect;
const chai = require('chai');
const env = require('dotenv').config()

const { deleteBooking } = require('../page/delete-booking')
const { createBooking } = require('../page/create-booking')

const testCase = {
    "positive" : {
       "deleteBookingSuccess" : "As an User, I should be able to create a booking"
    },
    "negative" : {
       "failedDeletedBooking" : "As a User, I should get error when I input blank field to create a booking",
       "failedInvalidAuth" : "As a User, I should get error when I input blank field to create a booking"
    }
   }

let credentials = {
    "username": 'admin',
    "password": 'password123'
}
let bookingID

describe(`@deleteBooking`, () => {

    describe(`Positive Case Delete Booking`, () => {

        beforeEach( async() => {

            const bodyRequest = {
                "firstname" : "Test",
                "lastname" : "Coba Mekari",
                "totalprice" : 12000,
                "depositpaid" : true,
                "bookingdates" : {
                    "checkin" : "2018-01-01",
                    "checkout" : "2019-01-01"
                },
                "additionalneeds" : "Breakfast"
            }
            
        const createResponse = await createBooking(bodyRequest)
        bookingID = createResponse.body.bookingid
        console.log('Booking ID: ' + bookingID)
        })

        it(`@del ${testCase.positive.deleteBookingSuccess}`, async() => {
            const deleteResponse = await deleteBooking(credentials.username, credentials.password, bookingID)
            console.log('Deleted Booking ID: ' + bookingID)
            assert(deleteResponse.status).to.equal(201);
            assert(deleteResponse.text).to.equal('Created');
        })
    
    })

    describe(`Negative Case Delete Booking`, () => {

        it(`@del ${testCase.negative.failedDeletedBooking}`, async() => {
            const deleteResponse = await deleteBooking(credentials.username, credentials.password, bookingID)
            console.log('Already Deleted Booking ID: ' + bookingID)
            assert(deleteResponse.status).to.equal(405);
            assert(deleteResponse.text).to.equal('Method Not Allowed');
        })

        it(`@del ${testCase.negative.failedInvalidAuth}`, async() => {
            credentials = {
                "username": 'asdf',
                "password": 'asdf123'
            }

            const deleteResponse = await deleteBooking(credentials.username, credentials.password, bookingID)
            console.log('Incorrect Auth Username: ' + credentials.username)
            console.log('Incorrect Auth Password: ' + credentials.password)
            assert(deleteResponse.status).to.equal(403);
            assert(deleteResponse.text).to.equal('Forbidden');
        })
    
    })
})