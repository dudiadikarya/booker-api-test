#  API Automation Testing with Mocha & Chai

---


### Note From Author:
Dear, Mekari Recruitment Team and kak Edo! 

I indeed have read the 'How to' file on working this Test, and I've noticed that you require to work on cucumber-ruby. However, in this short time, I realized that I don't have the capabilities to learn a complete new programming language in such a short time, I've tried to learn and work on ruby however but I got stuck and decided to use the programming language that I'm most comfortable with. If maybe you want to check the repo of me trying to learn cucumber-ruby I've added kak Edo as a collaborator there, and here's the repo link: https://bitbucket.org/dudiadikarya/booker-ruby-cucumber

I decided to use **Mocha&Chai** for this API Automation Test.

I'd appreciate it so much if you still want to check my work, but if it **has to use** cucumber-ruby feel free to ignore this work!

Thank you for your time and attention!

Regards,

Dudi Adikarya

---

### Prerequisites:

- Node.js
- VScode or other Text Editor.

### Get Started:
Clone this project repository into your local:
```sh
$ git clone https://dudiadikarya@bitbucket.org/dudiadikarya/booker-api-test.git
```

After you have successfully cloned this project repository, do the following:

```sh
$ npm install
```

### How to Run:

```sh
$ npm run test-api-win                              | to run all tests (Windows)
$ npm run test-api-win -- --grep @tag               | to run test with specific tag (Windows)

$ npm run test-api                              | to run all tests (Mac)
$ npm run test-api -- --grep @tag               | to run test with specific tag (Mac)

$ npm run reports                               | to generate mochawesome report
```
